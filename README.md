# awesome-tech-maps

A curated meta list of technology maps providing an overview of current technologies.

## Radars
Radars follow the idea of [thoughtworks](https://www.thoughtworks.com/) and typically categorize technologies into 4 categories:
- Hold: established technologies that are not recommended any more for new projects
- Assess: technologies that might become relevant in the future and/or might be relevant for specific domains already
- Trial: advanced technologies that should be further investigated on and might be used on a trial basis
- Adops: technologies that are highly recommended to be used

Each radar might have different categories and/or different definitions thereof.

- [Technology Radar](https://www.thoughtworks.com/radar/) by thoughtworks
- [Tech Radar](https://opensource.zalando.com/tech-radar/) by Zalando ([source code](https://github.com/zalando/tech-radar))


## Landscapes
Landscapes categorize recommened technologies on a table-like visualization.

- [CNCF Cloud Native Interactive Landscape](https://landscape.cncf.io/) by CNCF

## Awesome lists
Awesome lists are simple lists on various topics.

- [awesome (list of awesome lists)](https://github.com/sindresorhus/awesome)
- [awesome-software-architecture](https://github.com/simskij/awesome-software-architecture#readme)
- [awesome-egov-de](https://github.com/codedust/awesome-egov-de#readme)

